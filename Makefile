outputname=main.pdf

all: $(outputname)
$(outputname): main.tex
	latexmk -pdf -pdflatex="xelatex -8bit --shell-escape -interaction=nonstopmode" -use-make main.tex
clean:
	latexmk -c -C
	rm -fr *.csv
	rm compressed_$(outputname) -fr
	rm _minted-main -rf
